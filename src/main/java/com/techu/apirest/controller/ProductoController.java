package com.techu.apirest.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.security.AuthHandler;
import com.techu.apirest.security.JWTBuilder;
import com.techu.apirest.service.ProductoService;
import org.apache.tools.ant.types.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/apitechu/v2/")
public class ProductoController {

    @Autowired
    ProductoService productoService;
    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        return jwtBuilder.generateToken(name,"admin");
    }

    @GetMapping(path="/hello",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public String helloWorld(){
        String s = "Hello from JWT demo...";
        return s;
    }

    @GetMapping("/saludar")
    @Before(@BeforeElement(AuthHandler.class))
    public String saludar(){

        String mess = "hola";
        return mess;
    }


    @GetMapping("/productos")
    @Before(@BeforeElement(AuthHandler.class))
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    @Before(@BeforeElement(AuthHandler.class))
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    @Before(@BeforeElement(AuthHandler.class))
    public ProductoModel postProducto(@RequestBody ProductoModel nuevoProducto){
        return productoService.save(nuevoProducto);
    }

    @PutMapping("/productos")
    @Before(@BeforeElement(AuthHandler.class))
    public boolean putProducto( @RequestBody ProductoModel productoModificar){
        if (productoService.findById(productoModificar.getId()).isPresent()) {
            productoService.save(productoModificar);
            return true;
        }
        return false;
    }

    @DeleteMapping("/productos/{id}")
    @Before(@BeforeElement(AuthHandler.class))
    public boolean deleteProducto( @RequestBody ProductoModel productoToDelete, @RequestParam String id){

        if (productoService.findById(id).isPresent()) {
            return productoService.delete(productoToDelete);
        }
            return false;
    }

     @PatchMapping("/productos/{id}")
     @Before(@BeforeElement(AuthHandler.class))
     public ProductoModel patchProducto(@PathVariable String id,@RequestBody ProductoModel datosModificar) {
         if (productoService.findById(id).isPresent()) {
             return productoService.savePatch(id,datosModificar);
         }
         return null;
    }
}
