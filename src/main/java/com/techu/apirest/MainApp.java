package com.techu.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication ()
@ComponentScan(basePackages = {"com.kastkode.springsandwich.filter","com.techu.apirest"})
public class MainApp {

    public static void main(String[] args) {

        SpringApplication.run(MainApp.class, args);
    }
}
