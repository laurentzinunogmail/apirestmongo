package com.techu.apirest.service;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    // Que spring los inicialice cuando llegue el momento.
    // Spring hasta que no lo usemos no lo creará el objeto.
    @Autowired
    ProductoRepository productoRepository;

    //READ
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    //READ BY ID
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    //CREATE
    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
    }

    //DELETE
    public boolean delete (ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    public ProductoModel savePatch(String idModificar, ProductoModel datosModificar) {
        ProductoModel productoModificado = new ProductoModel();
        productoModificado = productoRepository.findById(idModificar).get();
        productoModificado.setDescripcion(datosModificar.getDescripcion());
        productoModificado.setPrecio(datosModificar.getPrecio());
        productoRepository.save(productoModificado);
        return productoModificado;
    }
}
