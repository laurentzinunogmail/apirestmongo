package com.techu.apirest;

import com.techu.apirest.controller.ProductoController;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ProductoControllerTest {
    @Test
    public void testCaseProductoController(){
        ProductoController controller = new ProductoController();
        assertEquals("Saludos desde TechU!",controller.saludar());

    }
}
